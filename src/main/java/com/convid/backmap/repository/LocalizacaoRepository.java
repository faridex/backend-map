package com.convid.backmap.repository;

import com.convid.backmap.model.Localizacao;

import org.springframework.data.repository.CrudRepository;

/**
 * LocalizacaoRepository
 */

public interface LocalizacaoRepository extends CrudRepository<Localizacao, Long>{


    
}