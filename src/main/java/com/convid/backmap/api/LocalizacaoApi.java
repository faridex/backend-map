package com.convid.backmap.api;

import java.util.List;

import com.convid.backmap.model.Localizacao;
import com.convid.backmap.service.LocalizacaoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * LocalizacaoApi
 */
@RestController
@RequestMapping("/localizacao-ocorrencia/v1")
public class LocalizacaoApi {

    @Autowired
    private LocalizacaoService service;

    @GetMapping("/")
    @CrossOrigin
    public List<Localizacao> getAllLocalizacao(){
        return service.getLocalizacoes();


    }
    /** teste do teste
    */

    @PostMapping("/")
    @CrossOrigin
    public void incluirLocalizacao(@RequestBody Localizacao localizacao){
         service.adicionarLocalizacao(localizacao);


    }
    
    
}
