package com.convid.backmap.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfig extends RouteBuilder {
    
    @Override
    public void configure() throws Exception {
        restConfiguration()
                .component("servlet")
                .enableCORS(true)
                .bindingMode(RestBindingMode.json)
                //Enable swagger endpoint.
                .apiContextPath("/swagger") //swagger endpoint path
                .apiContextRouteId("swagger") //id of route providing the swagger endpoint

                //Swagger properties
                //.contextPath("/api/v1") //base.path swagger property; use the mapping path set for CamelServlet
                .apiProperty("api.title", "Backend Mapeamento")
                .apiProperty("api.version", "1.0")
                .apiProperty("api.contact.name", "Farid Ahmad")
                .apiProperty("api.contact.email", "fahmad@redhmat.com")
                .apiProperty("host", "") //by default 0.0.0.0
                .apiProperty("port", "8080")
                .apiProperty("schemes", "")
        ;
    }
}