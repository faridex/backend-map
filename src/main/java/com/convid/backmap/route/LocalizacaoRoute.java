package com.convid.backmap.route;

import com.convid.backmap.model.Localizacao;
import com.convid.backmap.processor.LocalizacaoProcessor;
import com.convid.backmap.processor.LocalizacaoWithCacheProcessor;
import com.convid.backmap.service.LocalizacaoService;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class LocalizacaoRoute extends RouteBuilder {

    @Autowired
    private LocalizacaoService service;
    
    @Autowired
    @BeanInject
    private LocalizacaoProcessor processor;

    @Autowired
    @BeanInject
    private LocalizacaoWithCacheProcessor processorWithCache;

	@Override
	public void configure() throws Exception {
                        
                rest().description("Endpoint responsavel por gerenciar as localizações.")
                
                .get("/service-info/v1").description("Exibi dados de informções sobre a build em execução") 
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(() -> new com.convid.backmap.model.Info())
                .endRest()

                .get("/service-info").description("Exibi dados de informções sobre a build em execução") 
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(() -> new com.convid.backmap.model.Info())
                .endRest()

                .get("/info").description("Exibi dados de informções sobre a build em execução") 
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(() -> new com.convid.backmap.model.Info())
                .endRest()

                .get("/localizacao").description("Lista todas as localizações cadastradas na base de dados.") 
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(() -> service.getLocalizacoes())
                .endRest()
                        
                .get("/localizacao-wait").description("Lista todas as localizações cadastradas na base de dados.") 
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(() -> service.getLocalizacoesWait())
                .endRest()

                .get("/localizacao-cache").description("Lista todas as localizações a partir do cache.")
                .produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(() -> service.getLocalizacoesWithCache())
                .endRest()

                .post("/localizacao").description("Inclui uma nova Localização")
                .consumes(MediaType.APPLICATION_JSON_VALUE).type(Localizacao.class). outType(Localizacao.class)
                .route().process(processor)
                .endRest()
                        
                .post("/localizacao-cache").description("Incluir uma nova localização e aciona direciona esse regitro para o cache ")
                .consumes(MediaType.APPLICATION_JSON_VALUE).type(Localizacao.class). outType(Localizacao.class)
                .route().process(processorWithCache)
                .endRest(); 
            
	}


    
}
