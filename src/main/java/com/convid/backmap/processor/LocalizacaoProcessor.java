package com.convid.backmap.processor;

import com.convid.backmap.model.Localizacao;
import com.convid.backmap.service.LocalizacaoService;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * LocalizacaoProcessor
 */
@Component
public class LocalizacaoProcessor implements Processor{

    @Autowired
	private LocalizacaoService service;

	@Override
	public void process(Exchange exchange) throws Exception {
       service.adicionarLocalizacao(exchange.getIn().getBody(Localizacao.class));
	}
}