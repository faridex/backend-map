package com.convid.backmap.model;

public class Info {
    private String dataBuild;
    private String build;

    public Info(){
        dataBuild = "24/05/2020";
        build = "v.1.0.19";
    } 

    public String getDataBuild() {
        return dataBuild;
    }

    public void setDataBuild(String dataBuild) {
        this.dataBuild = dataBuild;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }


    
}