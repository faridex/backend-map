package com.convid.backmap.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SwaggerApi {
    @RequestMapping("/api/swagger-ui")
    public String redirectToUi() {
        return "redirect:/webjars/swagger-ui/index.html?url=/covid-backend/api/swagger&validatorUrl=";
    }
}