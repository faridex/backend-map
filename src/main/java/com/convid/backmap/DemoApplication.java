package com.convid.backmap;


import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.convid.backmap.helper.ServiceHelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
@EnableCaching
public class DemoApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	//	ServiceHelper.getHostName();
		

	}


}
