package com.convid.backmap.helper;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * ServiceHelper
 */
public class ServiceHelper {
    private static String _hostname;

    public static String getHostName(){
        try{
            if(_hostname == null){
                Process hostname = Runtime.getRuntime().exec("echo $HOSTNAME");
                BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(hostname.getInputStream()));
                String s;
                while ((s = stdInput.readLine()) != null) {
                    _hostname = s;
                }
                
            } 
            
        }catch(Exception e){}
        
        return _hostname;
    }
    
}