package com.convid.backmap.service;

import java.util.ArrayList;
import java.util.List;

import com.convid.backmap.helper.ServiceHelper;
import com.convid.backmap.model.Localizacao;
import com.convid.backmap.repository.LocalizacaoRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * LocalizacaoService
 */
@Service
public class LocalizacaoService {
    
    @Autowired
    private LocalizacaoRepository repository;

    @Autowired
    private SimpMessagingTemplate template;

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    
    
    public void load(){
        
        
    }

    public void adicionarLocalizacao(Localizacao localizacao){
        registrarLocalizacao(localizacao);
    }

    //@CacheEvict(allEntries = true,cacheNames = "localizacao-cache") 
    public void adicionarLocalizacaoWithCache(Localizacao localizacao){
         
        registrarLocalizacao(localizacao);
        
    }

    private void registrarLocalizacao(Localizacao localizacao){
        repository.save(localizacao);
        //template.convertAndSend("/topic/notification", localizacao);
    }
    
    public List<Localizacao> getLocalizacoes(){
        Iterable<Localizacao> io = repository.findAll();
        List<Localizacao> objects = new ArrayList<Localizacao>();
        io.forEach(objects::add);    
        
        for(Localizacao loc: objects){
            logger.info("Latitude "+ loc.getLatitude() + " Longitude "+loc.getLongitude() + " host "+ServiceHelper.getHostName());
        }

        return objects;
    }

    public List<Localizacao> getLocalizacoesWait(){
        Iterable<Localizacao> io = repository.findAll();
        List<Localizacao> objects = new ArrayList<Localizacao>();
        io.forEach(objects::add);    
        
        for(Localizacao loc: objects){
            logger.info("Latitude "+ loc.getLatitude() + " Longitude "+loc.getLongitude() + " host "+ServiceHelper.getHostName());
        }
        try{
            Thread.sleep(4000);
        }catch(Exception e){
            e.printStackTrace();
        }
        

        return objects;
    }
 
    //@Cacheable("localizacao-cache")
    public List<Localizacao> getLocalizacoesWithCache(){
        Iterable<Localizacao> io = repository.findAll();
        List<Localizacao> objects = new ArrayList<Localizacao>();
        io.forEach(objects::add);
        return objects;
    }
    
}